#!/bin/bash

# Script for QuadtreeAmogufier
# https://github.com/snailcon/QuadtreeAmogufier

cd "$(dirname $0)"

if [[ $1 == clean ]]; then
    rm -f audio.ogg in/*.png out/*.png
    exit
elif [[ ! $1 || ! $2 ]]; then
    echo "./amogufy.sh <in.mp4> <out.mp4> (BW | Col) (SFRC) (framerate)"
    echo "* (BW | Col), (SFRC), and (framerate) are optional"
    exit
fi

[[ ! $3 ]] && bwcl=BW || bwcl=$3
[[ ! $4 ]] && sfrc=2  || sfrc=$4
[[ ! $5 ]] && rate=30 || rate=$5
frames=$(ffprobe -v error -select_streams v:0 -count_packets -show_entries stream=nb_read_packets -of csv=p=0 "$1")

# compile amogufier
if [[ ! -e QuadtreeAmogufier ]]; then
    g++ -c Image.cpp main.cpp
    g++ -pthread Image.o main.o -o QuadtreeAmogufier
    rm -f *.o
fi

# convert in.mp4 to in frames
ffmpeg -i "$1" in/img_%01d.png

# amogufy (sus)
./QuadtreeAmogufier $bwcl 1 $frames $sfrc

# extract audio from in.mp4
ffmpeg -i "$1" -vn -acodec libvorbis -y audio.ogg

# create out.mp4
ffmpeg \
    -framerate $rate -i out/img_%01d.png   \
    -i audio.ogg -c:a copy -shortest       \
    -c:v libx264 -r $rate -pix_fmt yuv420p \
    "$2"
